# 24i Roku Assessment

## Disclaimers

This assessment was completed **without** the use of templates or off-the-shelf solutions. The only external code included is the TrackerTask.xml, which is required to use Roku's RALE tool.

While there were plenty of utilities written for this, it isn't my first time writing any of them. Most of the boilerplate I wrote for this app, I have written several times before.

## Outstanding Questions

1. How often should the heartbeat POST? Should it stop when the video pauses or stops?

## Devices Tested

1. Roku Ultra
2. Roku Express

## Running the App

1. Start the server with `cd server && java -jar resty2-1.0.0-SNAPSHOT.jar`
2. Open a terminal and run `ip config`
3. Record your ipv4 address
4. Open config.brs and modify the `api_address` entry with your ipv4 address where indicated.

### Deep Linking

Both app-launch and in-app deep links are supported.

### Config Flags

`api_address`: this needs to be updated with the address of the API (see above instructions)

`mock_missing_data`: this negatively impacts task response times, however it allows you to experience the app with mocked asset descriptions and years. If you turn this on, be patient with `/vod` requests.

`play_drm_stream`: allows you to toggle between the DASH and HLS streams.

`heartbeat_interval_seconds`: allows you to adjust how frequently the video heartbeat is sent. Couldn't find information on how frequently these should be sent, so 10 seconds was used.

### Manifest Const Flags

`debug=`: Enable debug logs. Error and debug logs cannot be turned off.

`log_warn=`: Enable warnings.

`log_info=`: Enable info logs.

`log_trace=`: Enable trace logs.

## Overview

### Areas for Improvement

1. Loading spinners sure would be nice
2. The VOD Task should be updated to perform asynchronous requests using a pool and allow requests to be terminated mid-flight if the user navigates away. At present, requests are synchronous and only one additional request can be queued.
3. BaseComponent and BaseScreen can be made more robust, such that they have an automated loading spinner while data is loaded. I started to work on this, but deprioritized it to get other stuff done.
4. HDMIStatus should be observed to pause video and present a dialog.
5. Link status should be observed to present a connection error dialog rather than letting requests timeout.

### Known Issues

1. If you deep link to the detail screen, or to the video screen and press back, you cannot use the left/right keys to navigate to adjacent assets.
2. Data for asset years and descriptions was unavailable. Random generators were created to accomodate this (see Config Flags).

### File Organization

```json
{
  "/.vsCode": "IDE settings. It's helpful for code style consistency if these settings are shared with the repo :)",
  "/roku": {
    "/components": {
      "/base": "Contains a few wrappers for adding application-wide script includes and logic.",
      "/libraries": "External code.",
      "/modules": "Non-UI components, only contains Router at the moment. Counterpart to /ui.",
      "/scene": "",
      "/screens": "Contains all of the application's screens.",
      "/tasks": "Contains app-wide tasks.",
      "/ui": "Contains app-wide UI components. Counterpart to /modules."
    },
    "/images": "",
    "/source": {
      "/utils": "Placed within /source so that it is available to main.brs."
    },
    ".env": "Used with the VSCode Brightscript plugin (see: launch.json) to speed up deploying side-loaded apps.",
    ".manifest": ""
  },
  "/server": "contains the provided server JAR."
}
```
