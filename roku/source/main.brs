'*************************************************************
'** 24i Roku Assessment
'** Copyright (c) 2020 Addison Short, All rights reserved.
'*************************************************************

'''''''''
' Main:
'
' @param {} [launchArgs=invalid]
'''''''''
sub Main(launchArgs = invalid)

  deepLink = parseDeepLink(launchArgs)

  LOG_INFO("App launched", deepLink, "main.brs", LINE_NUM)
  showApp(deepLink)

end sub

'''''''''
' parseDeepLink:
'
' @param {} launchArgs
' @return
'''''''''
function parseDeepLink(launchArgs)
  if isValid(launchArgs) and isValid(launchArgs.contentId) and isValid(launchArgs.mediaType)
    return {
      contentId: launchArgs.contentId
      mediaType: launchArgs.mediaType
    }
  end if
  return invalid
end function

'''''''''
' showApp:
'
' @param {} deepLink
'''''''''
sub showApp(deepLink)

  screen = CreateObject("roSGScreen")
  m.port = CreateObject("roMessagePort")
  screen.setMessagePort(m.port)

  scene = screen.CreateScene("HomeScene")
  scene.backExitsScene = false

  setupGlobalNode(screen)
  loadAppConfig(scene)

  if isValid(deepLink)
    scene.setField("deepLink", deepLink)
  end if

  hdmiStatus = createObject("roHdmiStatus")
  hdmiStatus.setMessagePort(m.port)

  inputObject = createObject("roInput")
  inputObject.setMessagePort(m.port)

  deviceInfo = createObject("roDeviceInfo")
  deviceInfo.enableLinkStatusEvent(true)
  deviceInfo.setMessagePort(m.port)

  ' show the screen!
  screen.show()

  ' scene observers must be set *after* screen.show() is called and before the while loop
  scene.observeField("exitApp", m.port)

  ' vscode_rale_tracker_entry
  ' above comment is used by the VSCode Brightscript plugin

  while true

    msg = wait(0, m.port)
    msgType = type(msg)

    if "roSGScreenEvent" = msgType
      if msg.isScreenClosed()
        LOG_INFO("Exiting app", invalid, "main.brs", LINE_NUM)
        return
      end if

    else if "roSGNodeEvent" = msgType

      if "exitApp" = msg.getField() and true = msg.getData()
        LOG_INFO("Exiting app", invalid, "main.brs", LINE_NUM)
        return
      end if

    else if "roHdmiStatusEvent" = msgType

      scene.setField("hdmiStatus", msg.getInfo().plugged)

    else if "roDeviceInfoEvent" = msgType

      scene.setField("deviceInfoEvent", msg.getInfo())

    else if "roInputEvent" = msgType

      if msg.isInput()

        deepLink = parseDeepLink(msg.getInfo())

        if isValid(deepLink)
          scene.setField("deepLink", deepLink)
        end if

      end if

    end if

  end while

end sub

'''''''''
' setupGlobalNode:
'
' @param {} screen
'''''''''
sub setupGlobalNode(screen)
  m.global = screen.getGlobalNode()
  m.global.setField("id", "GlobalNode")
end sub

'''''''''
' loadAppConfig: loads static app configuration data from config.brs or config_override.brs
'
'''''''''
sub loadAppConfig(scene)
  config = appConfig()

  if isFunction(appConfigOverride)
    config.append(appConfigOverride())
  end if

  GLOBAL_setField("appConfig", config)
end sub