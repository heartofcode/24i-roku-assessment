'''''''''
' config: static configuration file.
'
' Developers that want to override specific configs should create
' a "config.override.brs" file with a function "config_override" to supply the overrides. The config.override.brs file is ignored by git, so there is no risk of accidentally committing config changes to production.
'
' @return
'''''''''
function appConfig()
  return {
    api_address: "http://[your server ip]:8080"
    mock_missing_data: false ' warning: this will dramatically slow down the task response processing times
    play_drm_stream: true
    heartbeat_interval_seconds: 10
  }
end function