
'''''''''
' SCENE_getHomeScene:
'
' @return
'''''''''
function SCENE_getHomeScene()
  return m.top.getScene()
end function

'''''''''
' SCENE_findNode:
'
' @param {String} id
' @return
'''''''''
function SCENE_findNode(id as string)
  return SCENE_getHomeScene().findNode(id)
end function

'''''''''
' SCENE_showSpinner:
'
' @param {} [show=true]
' @return
'''''''''

#if false ' incomplete, deprioritized
  function SCENE_showSpinner(show = true)

    loader = SCENE_findNode("Loader")

    if isValid(loader)

      if show
        loader.control = "start"

      else
        loader.control = "stop"

      end if

    end if

  end function
#end if