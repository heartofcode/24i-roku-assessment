
function RANDOM_year(format = "YYYY")
  year = ""

  for i = 1 to len(format)
    letter = mid(format, i, 1)
    year += iif("Y" = letter, rnd(9).toStr(), letter)
  end for

  return year
end function

function RANDOM_paragraphs(paragraphs = 1 as integer)

  loremIpsum = ""
  for i = 1 to paragraphs
    loremIpsum += RANDOM_paragraph()
    loremIpsum += chr(10) + chr(10)
  end for

  return loremIpsum

end function

function RANDOM_paragraph()

  paragraph = ""
  for i = 0 to (2 + rnd(5))
    paragraph += RANDOM_sentence() + " "
  end for

  return paragraph

end function

function RANDOM_sentence()

  sentence = RANDOM_word(true)
  for i = 0 to (5 + rnd(10))
    sentence += " " + RANDOM_word()
  end for
  sentence += "."

  return sentence

end function

function RANDOM_word(uppercase = false)

  wordBank = iif(uppercase, RANDOM_wordBankUppercase(), RANDOM_wordBankLowercase())
  return wordbank[rnd(wordbank.count() - 1)]

end function

function RANDOM_wordBankUppercase()

  return ["Lorem", "Habitant", "Sit", "Ac", "Libero", "Non", "Amet", "Posuere", "Adipiscing", "Nulla", "a", "Consequat", "Amet",
  "Est", "Ullamcorper", "In", "Bibendum", "Egestas", "Quis", "Est", "Tellus", "Elit", "Non", "Donec", "At", "Eu", "Blandit", "Volutpat", "Quis", "Turpis", "Sagittis", "Velit", "Morbi", "Pulvinar", "Nulla", "Sed", "Sed", "Vulputate", "Enim", "a", "Sit", "Donec", "Euismod", "Quam", "Viverra", "Lectus", "Tristique", "Magna", "Gravida", "Vulputate", "Sed", "Ac", "Dignissim", "Fames", "Quisque", "Tellus", "a", "Tempus", "Sed", "Purus", "Malesuada", "Leo", "In", "Dolor", "Bibendum", "Dui",]

end function

function RANDOM_wordBankLowercase()

  return ["ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing", "elit", "sed", "do", "eiusmod", "tempor", "incididunt", "ut", "labore", "et", "dolore", "magna", "aliqua", "morbi", "tristique", "senectus", "et", "netus", "et", "malesuada", "fames", "ac", "amet", "venenatis", "urna", "cursus", "eget", "nunc", "scelerisque", "viverra", "turpis", "egestas", "maecenas", "pharetra", "convallis", "posuere", "morbi", "leo", "justo", "laoreet", "sit", "amet", "odio", "euismod", "lacinia", "at", "quis", "risus", "sed", "nulla", "facilisi", "morbi", "tempus", "iaculis", "lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "diam", "donec", "adipiscing", "tristique", "facilisi", "nullam", "vehicula", "ipsum", "a", "arcu", "interdum", "varius", "sit", "amet", "mattis", "vulputate", "enim", "nulla", "risus", "nullam", "eget", "felis", "eget", "nunc", "ante", "in", "nibh", "mauris", "cursus", "mattis", "molestie", "malesuada", "proin", "libero", "nunc", "consequat", "interdum", "varius", "sit", "amet", "egestas", "erat", "imperdiet", "sed", "euismod", "nisi", "porta", "at", "varius", "vel", "pharetra", "vel", "turpis", "sed", "tempus", "urna", "et", "pharetra", "pharetra", "massa", "massa", "ultricies", "eleifend", "quam", "adipiscing", "vitae", "proin", "sagittis", "nisl", "ultricies", "integer", "quis", "auctor", "elit", "sed", "vulputate", "mi", "sit", "in", "hac", "habitasse", "platea", "ut", "aliquam", "purus", "sit", "amet", "luctus", "venenatis", "enim", "praesent", "elementum", "facilisis",
  "massa", "sapien", "faucibus", "et", "molestie", "ac", "feugiat", "sed", "volutpat", "diam", "ut", "venenatis", "volutpat", "odio", "facilisis", "mauris", "aliquam", "etiam", "erat", "velit", "scelerisque", "in", "diam", "ut", "venenatis", "tellus", "in", "metus", "vulputate", "hendrerit", "dolor", "magna", "eget", "est", "lorem", "tincidunt", "id", "aliquet", "risus", "feugiat", "in", "ante", "metus", "dictum", "purus", "sit", "amet", "volutpat", "consequat", "mauris", "euismod", "in", "pellentesque", "massa", "placerat", "duis", "ultricies", "tincidunt", "ornare", "massa", "eget", "egestas", "purus", "viverra", "mattis", "nunc", "sed", "blandit", "libero", "volutpat", "sed", "cras", "ornare", "at", "volutpat", "diam", "ut", "venenatis", "tellus", "tempus", "urna", "et", "pharetra", "pharetra", "faucibus", "turpis", "in", "eu", "mi", "bibendum", "neque", "egestas", "dignissim", "suspendisse", "in", "est", "ante", "in", "nibh", "mauris", "cursus", "nunc", "faucibus", "a", "pellentesque", "sit", "amet", "porttitor", "amet", "luctus", "venenatis", "lectus", "magna", "fringilla", "ultrices", "tincidunt", "arcu", "non", "sodales", "neque", "in", "pellentesque", "massa", "placerat", "duis", "ultricies", "lacus", "sed", "lacus", "suspendisse", "faucibus", "interdum", "orci", "sagittis", "eu", "volutpat", "odio", "vestibulum", "mattis", "ullamcorper", "velit", "sed", "senectus", "et", "netus", "et", "malesuada", "fames", "ac", "fringilla", "urna", "porttitor", "rhoncus", "dolor", "purus", "arcu", "ac", "tortor", "dignissim", "dignissim", "suspendisse", "in", "est", "ante", "in", "nibh", "mauris", "cursus", "sed", "risus", "pretium", "quam", "vulputate", "dignissim", "suspendisse", "in", "tincidunt", "vitae", "semper", "quis", "lectus", "nulla", "at", "volutpat", "enim", "sit", "amet", "venenatis", "urna", "cursus", "eget", "nunc", "scelerisque", "ac", "turpis", "egestas", "maecenas", "egestas", "diam", "in", "arcu", "cursus", "euismod", "quis", "mauris", "a", "diam", "maecenas", "sed", "enim", "ut", "sem", "viverra", "egestas", "sed", "sed", "risus", "lectus", "vestibulum", "mattis", "ullamcorper", "velit", "sed", "ullamcorper", "non", "enim", "praesent", "elementum", "facilisis", "leo", "bibendum", "arcu", "vitae", "elementum", "curabitur", "vitae", "nunc", "duis", "ut", "diam", "quam", "cursus", "turpis", "massa", "tincidunt", "dui", "ut", "ornare", "lectus", "sit", "morbi", "non", "arcu", "risus", "quis", "varius", "quam", "quisque", "arcu", "vitae", "elementum", "curabitur", "vitae", "nunc", "sed", "velit", "dignissim", "faucibus", "in", "ornare", "quam"]

end function