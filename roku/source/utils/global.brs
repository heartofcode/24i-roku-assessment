
sub GLOBAL_setField(key, value, alwaysNotify = false)

  if m.global.hasField(key)
    m.global.setField(key, value)

  else
    obj = {}
    obj[key] = value
    m.global.addFields(obj)

  end if

end sub

sub GLOBAL_setFields(assocArray)

  for each set in assocArray.items()
    GLOBAL_setField(set.key, set.value)
  end for

end sub

function GLOBAL_getField(key)

  if isValid(m.global) and m.global.hasField(key)
    return m.global.getField(key)
  end if

  return invalid
end function