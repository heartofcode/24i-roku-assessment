sub FOCUS_init(array = [], initialFocus = -1)

  if not isArray(array)
    LOG_ERROR("FOCUS_init's first parameter must be an array of focusable nodes.")
    array = []
  end if

  STATE_setState({
    focusArray: array
    focusIndex: initialFocus
  })

  if - 1 < initialFocus
    array[initialFocus].setFocus(true)
  end if

end sub

sub FOCUS_pushNode(node)

  if isValid(m.state) and isArray(m.state.focusArray)
    m.state.focusArray.push(node)
  else
    FOCUS_init([node])
  end if

end sub

function FOCUS_restore()

  if isValid(m.state) and isValid(m.state.focusIndex)
    FOCUS_set(m.state.focusIndex)
  else
    FOCUS_set(0)
  end if

end function

function FOCUS_move(move = 0)

  return FOCUS_set(m.state.focusIndex + move)

end function

function FOCUS_set(index = 0)

  if isValid(m.state) and isValid(m.state.focusArray)

    boundedIndex = MATH_bound(0, index, m.state.focusArray.count() - 1)

    'if not m.state.focusArray[boundedIndex].isInFocusChain()

    STATE_setState({ focusIndex: boundedIndex })
    NODE_setFocus(m.state.focusArray[boundedIndex], true)

    LOG_TRACE("Set focus to ", m.state.focusArray[boundedIndex].getSubtype())

    return true

    'end if

  end if

  return false

end function