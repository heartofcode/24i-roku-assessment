
function MATH_bound(min, value, max)

  return MATH_max(MATH_min(value, max), min)

end function

function MATH_min(value1, value2)

  if not isNumber(value1) then return value2
  if not isNumber(value2) then return value1

  if value1 < value2 then return value1
  return value2

end function

function MATH_max(value1, value2)

  if not isNumber(value1) then return value2
  if not isNumber(value2) then return value1

  if value1 > value2 then return value1
  return value2

end function