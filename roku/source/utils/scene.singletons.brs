
function SCENE_getVODTask()
  vodTask = SCENE_findNode("VODTask")
  if not isValid(vodTask)
    vodTask = SCENE_getHomeScene().createChild("VODTask")
  end if
  return vodTask
end function

function SCENE_getRouter()
  router = SCENE_findNode("Router")
  if not isValid(router)
    router = SCENE_getHomeScene().createChild("Router")
  end if
  return router
end function