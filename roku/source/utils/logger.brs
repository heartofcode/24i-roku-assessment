
function LOG_getLevels()

  return {
    FATAL: -1,
    ERROR: 0,
    INFO: 1,
    DEBUG: 2,
    TRACE: 3
  }

end function

sub LOG_FATAL(message, value = invalid, filename = "", line = invalid)

  _LOG_MESSAGE(LOG_getLevels().fatal, "[FATAL]", message, value, filename, line)

end sub

sub LOG_ERROR(message, value = invalid, filename = m.top.getSubtype() + ".brs", line = invalid)

  _LOG_MESSAGE(LOG_getLevels().error, "[ERROR]", message, value, filename, line)

end sub

sub LOG_WARN(message, value = invalid, filename = m.top.getSubtype() + ".brs", line = invalid)

  _LOG_MESSAGE(LOG_getLevels().warn, "[WARN]", message, value, filename, line)

end sub

sub LOG_INFO(message, value = invalid, filename = m.top.getSubtype() + ".brs", line = invalid)

  _LOG_MESSAGE(LOG_getLevels().info, "[INFO]", message, value, filename, line)

end sub

sub LOG_TRACE(message, value = invalid, filename = m.top.getSubtype() + ".brs", line = invalid)

  _LOG_MESSAGE(LOG_getLevels().trace, "[TRACE]", message, value, filename, line)

end sub

sub _LOG_MESSAGE(level, levelString, message, value, filename, line)

  if _LOG_shouldPrint(level)

    ? levelString ;
    if isStringWithLength(filename)
      ? " " ; filename ;
      if invalid <> line
        ? ", LINE " ; line.toStr() ;
      end if
    end if
    if isStringWithLength(message)
      ? " : " ; message ;
    end if
    if isValid(value)
      if isAssociativeArray(value) or isNode(value)
        ? chr(10) ; value
      else
        ? " " ; value
      end if
    else
      ?
    end if

  end if

end sub

function _LOG_shouldPrint(level)

  logLevels = LOG_getLevels()

  if logLevels.fatal = level or logLevels.error = level

    ' always print fatal and error messages
    return true

  else if logLevels.warn = level

    #if log_warn
      return true
    #end if

  else if logLevels.info = level

    #if log_info
      return true
    #end if

  else if logLevels.trace = level

    #if log_trace
      return true
    #end if

  end if

  return false

end function