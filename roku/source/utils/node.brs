
sub NODE_setFocus(node, focus, filename = "", lineNum = invalid)

  if isValid(node)

    LOG_TRACE("Focus changed to " + focus.toStr() + " for ", node.getSubtype(), filename, lineNum)
    node.setFocus(focus)

  end if

end sub