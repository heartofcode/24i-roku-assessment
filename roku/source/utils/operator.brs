
'''''''''
' iif: simple inline if/else function
'
' @param {Boolean} testValue
' @param {Dynamic} validResult
' @param {Dynamic} invalidResult
' @return {dynamic}
'''''''''
function iif(testValue as boolean, validResult as dynamic, invalidResult as dynamic) as dynamic
  if true = testValue
    return validResult
  else
    return invalidResult
  end if
end function