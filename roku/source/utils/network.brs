
function NETWORK_get(url, headers = invalid)
  return NETWORK_fetch("GET", url, headers, invalid)
end function

function NETWORK_post(url, headers = invalid, body = invalid)
  return NETWORK_fetch("POST", url, headers, body)
end function

function NETWORK_fetch(verb, url, headers = invalid, body = invalid)
  url = url.encodeUri()

  LOG_TRACE("Network request: ", {
    verb: verb,
    url: url,
    headers: headers,
    body: body
  })

  request = CreateObject("roURLTransfer")
  request.setPort(CreateObject("roMessagePort"))
  request.setRequest(verb)

  if isAssociativeArray(headers)
    request.setHeaders(headers)
  end if

  request.setUrl(url)
  if - 1 <> url.instr(0, "https")
    request.setCertificatesFile("common:/certs/ca-bundle.crt")
  end if

  response = invalid

  if isAssociativeArray(body)
    request.retainBodyOnError(true)
    response = request.postFromString(FormatJson(body))
  else
    response = request.getToString()
  end if

  return response
end function

function NETWORK_addQueryParam(url, query)
  if - 1 = url.instr(0, "?") ' there is no question mark
    url += "?" + query
  else
    url += "&" + query
  end if

  return url
end function