
'''''''''
' center:
'
' @param {} node
' @param {} [divisor=[2, 2]]
' @param {} [relativeTo=invalid]
'''''''''
sub center(node, divisor = [2, 2], relativeTo = invalid)
  nodeBounds = node.boundingRect()
  parentBounds = iif(isValid(relativeTo), relativeTo, node.getParent()).boundingRect()

  node.translation = [
    (parentBounds.width - nodeBounds.width) / divisor[0],
    (parentBounds.height - nodeBounds.height) / divisor[1]
  ]
end sub

'''''''''
' centerHoriz:
'
' @param {} node
' @param {} [divisor=2]
' @param {} [relativeTo=invalid]
'''''''''
sub centerHoriz(node, divisor = 2, relativeTo = invalid)
  nodeBounds = node.boundingRect()
  parentBounds = iif(isValid(relativeTo), relativeTo, node.getParent()).boundingRect()

  node.translation = [
    (parentBounds.width - nodeBounds.width) / divisor,
    node.translation[1]
  ]
end sub

'''''''''
' centerVert:
'
' @param {} node
' @param {} [divisor=2]
' @param {} [relativeTo=invalid]
'''''''''
sub centerVert(node, divisor = 2, relativeTo = invalid)
  nodeBounds = node.boundingRect()
  parentBounds = iif(isValid(relativeTo), relativeTo, node.getParent()).boundingRect()

  node.translation = [
    node.translation[0],
    (parentBounds.height - nodeBounds.height) / divisor
  ]
end sub