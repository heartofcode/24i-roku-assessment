
'''''''''
' isValid: checks that the value is not uninitialized or invalid
'
' @param {Dynamic} value
' @return {boolean}
'''''''''
function isValid(value)
  return "<uninitialized>" <> type(value) and invalid <> value
end function

function isFunction(value as dynamic) as boolean
  return isValid(value) and GetInterface(value, "ifFunction") <> invalid
end function

function isNumber(value)
  return isValid(value) and (isInteger(value) or isFloat(value) or isDouble(value))
end function

function isInteger(value)
  valType = type(value)
  isInt = GetInterface(value, "ifInt") <> invalid and (valType = "roInt" or valType = "roInteger" or valType = "Integer")
  isLongInt = GetInterface(value, "ifLongInt") <> invalid and (valType = "roLongInt" or valType = "roLongInteger" or valType = "LongInteger")

  return isInt or isLongInt
end function

function isFloat(value)
  return isValid(value) and (GetInterface(value, "ifFloat") <> invalid or (type(value) = "roFloat" or type(value) = "Float"))
end function

function isDouble(value)
  return isValid(value) and (GetInterface(value, "ifDouble") <> invalid or (type(value) = "roDouble" or type(value) = "roIntrinsicDouble" or type(value) = "Double"))
end function

function isString(value)
  return isValid(value) and invalid <> GetInterface(value, "ifString")
end function

function isStringWithLength(value, length = 1)
  return isString(value) and value.len() >= length
end function

function isNode(value)
  return isValid(value) and "roSGNode" = type(value)
end function

function isAssociativeArray(value)
  return isValid(value) and invalid <> getInterface(value, "ifAssociativeArray") and "roSGNode" <> type(value)
end function

function isArray(value)
  return isValid(value) and invalid <> GetInterface(value, "ifArray")
end function

'''''''''
' isArrayWithLength
'
' @param {Dynamic} value
' @param {Integer} [length=1]
' @param {Boolean} [hasExactLength=false]
' @return {boolean}
'''''''''
function isArrayWithLength(value as dynamic, length = 1 as integer, hasExactLength = false as boolean) as boolean

  if length < 1
    ? "WARNING: Test when length is less than ", length, iff(length = 0, " not recommended (use isArray when zero-length is valid)", "not allowed (length is negative)")

    if length < 0 then return false

  end if

  if not hasExactLength
    return isArray(value) and value.count() >= length

  else
    return isArray(value) and value.count() = length

  end if

end function