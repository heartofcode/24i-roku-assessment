'''''''''
' init:
'
'''''''''
sub init()
  m.border = m.top.findNode("border")
  m.border.color = m.top.borderColor
  m.background = m.top.findNode("background")
  m.buttonText = m.top.findNode("buttonText")
  m.paddingLeft = m.top.findNode("paddingLeft")
  m.paddingRight = m.top.findNode("paddingRight")
  m.paddingTop = m.top.findNode("paddingTop")
  m.paddingBottom = m.top.findNode("paddingBottom")

  'setPadding(m.top.padding)
  'setWidth(m.top.width)

  m.top.observeField("focusedChild", "onFocus")
  m.top.observeField("padding", "onPadding")
  m.top.observeField("width", "onWidth")
end sub

'''''''''
' onFocus:
'
'''''''''
sub onFocus()
  if m.top.isInFocusChain()
    m.border.color = m.top.borderFocusColor
  else
    m.border.color = m.top.borderColor
  end if
end sub

'''''''''
' onKeyEvent:
'
' @param {} key
' @param {} press
' @return {boolean}
'''''''''
function onKeyEvent(key, press) as boolean
  handled = false

  if press
    if "OK" = key
      m.top.buttonSelected = true
      handled = true
    end if
  end if

  return handled
end function

'''''''''
' onWidth:
'
' @param {} event
'''''''''
sub onWidth(event)
  setWidth(event.getData())
end sub

'''''''''
' setWidth:
'
' @param {} width
'''''''''
sub setWidth(width)
  m.buttonText.maxWidth = width - m.top.padding[1] - m.top.padding[3]
  updateBackground()
end sub

'''''''''
' onPadding
'
' @param {} event
'''''''''
sub onPadding(event)
  setPadding(event.getData())
end sub

'''''''''
' setPadding:
'
' @param {} padding
'''''''''
sub setPadding(padding)
  if isArrayWithLength(padding, 4, true)
    m.paddingTop.height = padding[0]
    m.paddingRight.width = padding[1]
    m.paddingBottom.height = padding[2]
    m.paddingLeft.width = padding[3]
    updateBackground()
  else
    LOG_WARN("<Input /> padding value must be an array with exactly four values.")
  end if
end sub

'''''''''
' updateBackground:
'
'''''''''
sub updateBackground()
  boundingRect = m.top.boundingRect()

  m.background.update({
    translation: [m.top.borderSize, m.top.borderSize]
    width: boundingRect.width - (m.top.borderSize * 2)
    height: boundingRect.height - (m.top.borderSize * 2)
  })
  m.border.update({
    width: boundingRect.width
    height: boundingRect.height
  })
end sub