'''''''''
' init:
'
'''''''''
sub init()
  m.nodes = {
    wrapper: m.top.findNode("wrapper")
    image: m.top.findNode("image")
    title: m.top.findNode("title")
  }
  m.top.observeField("itemContent", "onItemContent")
  m.nodes.image.observeField("loadStatus", "onLoadStatus")
end sub

'''''''''
' onItemContent:
'
' @param {} event
'''''''''
sub onItemContent(event)
  itemData = event.getData()
  width = m.top.width.toStr()

  if isValid(itemData) and isValid(m.global.appConfig)
    m.nodes.title.update({
      text: itemData.title
      width: width
    })

    imageUri = m.global.appConfig.api_address + "/vod/" + itemData.id + "/poster"
    imageUri = NETWORK_addQueryParam(imageUri, "width=" + width)
    imageUri = NETWORK_addQueryParam(imageUri, "height=" + width) ' always a square image for this

    m.nodes.image.update({
      loadWidth: width
      loadHeight: width
    })
    m.nodes.image.uri = imageUri
  end if
end sub

'''''''''
' onLoadStatus:
'
' @param {} event
'''''''''
sub onLoadStatus(event)
  status = event.getData()
  if "failed" = status
    LOG_WARN("Image failed to load for URI: ", m.nodes.image.uri)
  end if
end sub