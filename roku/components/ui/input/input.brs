'''''''''
' init:
'
'''''''''
sub init()
  m.border = m.top.findNode("border")
  m.border.color = m.top.borderColor
  m.background = m.top.findNode("background")
  m.input = m.top.findNode("input")
  m.paddingLeft = m.top.findNode("paddingLeft")
  m.paddingRight = m.top.findNode("paddingRight")
  m.paddingTop = m.top.findNode("paddingTop")
  m.paddingBottom = m.top.findNode("paddingBottom")

  setPadding(m.top.padding)
  setWidth(m.top.width)

  m.top.observeField("focusedChild", "onFocus")
  m.top.observeField("padding", "onPadding")
  m.top.observeField("width", "onWidth")
end sub

'''''''''
' onFocus:
'
'''''''''
sub onFocus()
  if m.top.isInFocusChain()
    m.border.color = m.top.borderFocusColor
  else
    m.border.color = m.top.borderColor
  end if
end sub

'''''''''
' onKeyEvent:
'
' @param {} key
' @param {} press
' @return {boolean}
'''''''''
function onKeyEvent(key, press) as boolean
  handled = false

  if press
    if "OK" = key
      m.keyboardDialog = createKeyboardDialog({
        title: "Search" ' fun fact: if you don't include a title, the dialog renders partially off-screen
        buttons: ["Enter"]
        text: m.input.text
      })
      m.keyboardDialog.observeField("wasClosed", "inputEntered")
      handled = true
    end if
  end if

  return handled
end function

'''''''''
' createKeyboardDialog:
'
' @param {} [dialogConfig={}]
' @return {object}
'''''''''
function createKeyboardDialog(dialogConfig = {}) as object
  dialog = CreateObject("roSGNode", "KeyboardDialog")
  dialog.update(dialogConfig)
  dialog.observeField("buttonSelected", "keyboardButtonSelected")

  ' roku's native keyboard dialog always starts at the zero index,
  ' so when we open the dialog with text already set, we need to update the cursor position
  dialog.keyboard.textEditBox.cursorPosition = dialog.text.len()

  m.top.getScene().dialog = dialog
  return dialog
end function

'''''''''
' inputEntered:
'
' @param {} event
'''''''''
sub inputEntered(event)
  m.keyboardDialog.unobserveField("wasClosed")
  m.keyboardDialog.unobserveField("buttonSelected")
  m.keyboardDialog.buttonGroup.unobserveField("focusedChild")

  m.input.text = m.keyboardDialog.text
  m.keyboardDialog = invalid
end sub

'''''''''
' keyboardButtonSelected:
'
' @param {} event
'''''''''
sub keyboardButtonSelected(event)
  button = m.keyboardDialog.buttons[event.getData()]

  if "Enter" = button
    m.keyboardDialog.close = true
  end if
end sub

'''''''''
' onWidth:
'
' @param {} event
'''''''''
sub onWidth(event)
  setWidth(event.getData())
end sub

'''''''''
' setWidth:
'
' @param {} width
'''''''''
sub setWidth(width)
  m.input.maxWidth = width - m.top.padding[1] - m.top.padding[3]
  updateBackground()
end sub

'''''''''
' onPadding
'
' @param {} event
'''''''''
sub onPadding(event)
  setPadding(event.getData())
end sub

'''''''''
' setPadding:
'
' @param {} padding
'''''''''
sub setPadding(padding)
  if isArrayWithLength(padding, 4, true)
    m.paddingTop.height = padding[0]
    m.paddingRight.width = padding[1]
    m.paddingBottom.height = padding[2]
    m.paddingLeft.width = padding[3]
    updateBackground()
  else
    LOG_WARN("<Input /> padding value must be an array with exactly four values.")
  end if
end sub

'''''''''
' updateBackground:
'
'''''''''
sub updateBackground()
  m.background.update({
    translation: [m.top.borderSize, m.top.borderSize]
    width: m.top.boundingRect().width - (m.top.borderSize * 2)
    height: m.top.boundingRect().height - (m.top.borderSize * 2)
  })
  m.border.update({
    width: m.top.boundingRect().width
    height: m.top.boundingRect().height
  })
end sub