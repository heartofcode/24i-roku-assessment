'''''''''
' init:
'
'''''''''
sub init()
  m.state = {
    history: []
  }
end sub

'''''''''
' navigateTo: goes to a new screen
'
' @param {} [params={}] Supports "id" for the screen to render, "data" to pass it additonal information, and "clearHistory" to purge the history state before navigating.
'''''''''
sub navigateTo(params = {})
  if true = params.clearHistory
    clearHistory()
  else
    previousScreen = m.state.history.peek()
    if isValid(previousScreen)
      previousScreen.visible = false
    end if
  end if

  screen = m.top.createChild(params.id)
  m.state.history.push(screen)

  if isValid(params.data)
    screen.data = params.data
  end if

  screen.setFocus(true)
end sub

'''''''''
' back: Goes back in the screen history
'
' @param {} [params={}]
' @return
'''''''''
function back(params = {})
  success = false

  if m.state.history.count() > 1
    screen = m.state.history.pop()
    screen.getParent().removeChild(screen)

    previousScreen = m.state.history.peek()
    previousScreen.visible = true
    previousScreen.setFocus(true)

    success = true
  end if

  return success
end function

'''''''''
' clearHistory: deletes everything in the screen history
'
'''''''''
sub clearHistory()
  for each screen in m.state.history
    screen.getParent().removeChild(screen)
  end for
  STATE_setState({ history: [] })
end sub