'''''''''
' init:
'
'''''''''
sub init()
  m.nodes = {
    cover: m.top.findNode("cover")
    wrapper: m.top.findNode("wrapper")
    title: m.top.findNode("title")
    year: m.top.findNode("year")
    description: m.top.findNode("description")
    play: m.top.findNode("play")
    vodTask: SCENE_getVodTask()
  }
  m.state = {}
  m.top.observeField("data", "onData")
  m.top.observeField("focusedChild", "onFocus")
end sub

'''''''''
' onFocus:
'
'''''''''
sub onFocus()

  if m.top.hasFocus()
    FOCUS_restore()
  end if

end sub

'''''''''
' onData: handles normal data flow and deep links
'
' @param {} event
'''''''''
sub onData(event)
  data = event.getData()

  if isValid(data.assetIndex) and isValid(data.assets)
    asset = data.assets.getChild(data.assetIndex)
    populateScreen(asset)

  else if isValid(data.contentId) and isValid(data.mediaType)
    STATE_setState({ isDeepLink: true })
    m.nodes.vodTask.observeFieldScoped("response", "onVod")
    m.nodes.vodTask.request = {
      id: data.contentId
    }
  end if
end sub

'''''''''
' onVod: Asset is ready for display
'
' @param {} event
'''''''''
sub onVod(event)
  vod = event.getData()
  populateScreen(vod)
end sub

'''''''''
' populateScreen: Renders the screen
'
' @param {} asset
'''''''''
sub populateScreen(asset)
  m.nodes.title.text = asset.title
  m.nodes.description.text = asset.description
  m.nodes.year.text = asset.year

  if "movie" = asset.type
    m.nodes.play.text = "Play"
    m.nodes.play.visible = true
    m.nodes.play.observeField("buttonSelected", "onPlayButton")
    FOCUS_init([m.nodes.play], 0)

  else
    m.nodes.play.visible = false

  end if

  imageUri = m.global.appConfig.api_address + "/vod/" + asset.id + "/poster"
  imageUri = NETWORK_addQueryParam(imageUri, "width=" + m.nodes.cover.width.toStr())
  imageUri = NETWORK_addQueryParam(imageUri, "height=" + m.nodes.cover.height.toStr())
  m.nodes.cover.uri = imageUri

  center(m.nodes.wrapper, [2, 2], m.top)
end sub

'''''''''
' onPlayButton:
'
' @param {} event
'''''''''
sub onPlayButton(event)
  SCENE_getRouter().callFunc("navigateTo", {
    id: "PlayerScreen",
    data: { node: m.top.data.assets.getChild(m.top.data.assetIndex) }
  })
end sub

'''''''''
' onKeyEvent:
'
' @param {} key
' @param {} press
' @return {boolean}
'''''''''
function onKeyEvent(key, press) as boolean
  handled = false

  if press

    if "back" = key and true = m.state.isDeepLink
      SCENE_getRouter().callFunc("navigateTo", { id: "GridScreen", clearHistory: true })
      handled = true

    else if "left" = key

      nextAssetIndex = m.top.data.assetIndex - 1
      if nextAssetIndex <> -1
        SCENE_getRouter().callFunc("navigateTo", {
          id: "DetailScreen",
          data: {
            assetIndex: nextAssetIndex
            assets: m.top.data.assets
          }
        })
        handled = true
      end if

    else if "right" = key

      nextAssetIndex = m.top.data.assetIndex + 1
      if nextAssetIndex < m.top.data.assets.getChildCount() - 1
        SCENE_getRouter().callFunc("navigateTo", {
          id: "DetailScreen",
          data: {
            assetIndex: nextAssetIndex
            assets: m.top.data.assets
          }
        })
        handled = true

      end if

    end if

  end if

  return handled
end function