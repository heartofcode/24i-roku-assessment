'''''''''
' init:
'
'''''''''
sub init()
  m.nodes = {
    grid: m.top.findNode("grid")
  }

  FOCUS_init([m.nodes.grid])

  ' observers
  m.top.observeField("focusedChild", "onFocus")
  m.nodes.grid.observeField("itemSelected", "onAssetSelected")

  show()
end sub

'''''''''
' onFocus:
'
'''''''''
sub onFocus()
  if m.top.hasFocus()
    FOCUS_restore()
  end if
end sub

'''''''''
' prepare: runs necessary data fetching prior to rendering
'
'''''''''
sub prepare()
  m.nodes.vodTask = SCENE_getVODTask()
  m.nodes.vodTask.control = "run"
  m.nodes.vodTask.observeFieldScoped("response", "onVODResponse")
  m.nodes.vodTask.request = {
    search: ""
  }
  m.top.ready = true
end sub

'''''''''
' onKeyEvent:
'
' @param {} key
' @param {} press
' @return {boolean}
'''''''''
function onKeyEvent(key, press) as boolean
  handled = false

  if press
    if "options" = key
      SCENE_getRouter().callFunc("navigateTo", { id: "SearchScreen" })
      handled = true
    end if
  end if

  return handled
end function

'''''''''
' onVODResponse: renders the page with asset data
'
' @param {} event
'''''''''
sub onVODResponse(event)
  m.nodes.vodTask.unobserveFieldScoped("response")
  responseNode = event.getData()
  m.nodes.grid.content = responseNode

  center(m.nodes.grid, [2, 2], m.top.getScene())
end sub

'''''''''
' onAssetSelected: handles going to asset detail screen
'
' @param {} event
'''''''''
sub onAssetSelected(event)
  selectedIndex = event.getData()
  selectedItem = m.nodes.grid.content.getChild(selectedIndex)
  LOG_TRACE("onAssetSelected()", selectedIndex)

  SCENE_getRouter().callFunc("navigateTo", {
    id: "DetailScreen",
    data: {
      assetIndex: selectedIndex
      assets: m.nodes.grid.content
    }
  })
end sub