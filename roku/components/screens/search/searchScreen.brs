'''''''''
' init:
'
'''''''''
sub init()

  ' nodes
  m.nodes = {
    wrapper: m.top.findNode("wrapper")
    search: m.top.findNode("search")
    filters: m.top.findNode("filters")
    results: m.top.findNode("results")
  }

  ' state
  m.state = {
    requiresPrep: false
  }

  FOCUS_init([m.nodes.search, m.nodes.filters], 0)

  m.nodes.wrapper.translation = [1920 / 2, 0]
  centerVert(m.nodes.wrapper, 4, m.top.getScene())

  ' observers
  m.top.observeField("focusedChild", "onFocus")
  m.nodes.search.observeField("text", "onSearch")
  m.nodes.filters.observeField("itemSelected", "onFilterSelected")
  m.nodes.results.observeField("rowItemSelected", "onResultSelected")

  show()
end sub

function onKeyEvent(key, press) as boolean
  handled = false

  if press
    if "up" = key
      handled = FOCUS_move(-1)
    else if "down" = key
      handled = FOCUS_move(1)
    end if
  end if

  return handled
end function

'''''''''
' onFocus:
'
'''''''''
sub onFocus()
  if m.top.hasFocus()
    FOCUS_restore()
  end if
end sub

sub prepare()
  m.nodes.vodTask = SCENE_getVODTask()
  m.nodes.vodTask.control = "run"
  m.top.ready = true
end sub

'''''''''
' onSearch:
'
' @param {} event
'''''''''
sub onSearch(event)
  search = event.getData()
  LOG_TRACE("onSearch()", search)

  STATE_setState({ search: search })

  m.nodes.vodTask.observeFieldScoped("response", "onSearchResponse")
  m.nodes.vodTask.request = {
    search: m.state.search
    filter: m.state.filter
  }
end sub

sub onFilterSelected(event)
  selectedIndex = event.getData()
  selectedItem = m.nodes.filters.content.getChild(selectedIndex)
  LOG_TRACE("onFilterSelected()", selectedIndex)

  if isValid(selectedItem)

    filter = selectedItem.title
    if "All" = filter then filter = invalid

    STATE_setState({ filter: filter })

    m.nodes.vodTask.observeFieldScoped("response", "onSearchResponse")
    m.nodes.vodTask.request = {
      search: m.state.search
      filter: m.state.filter
    }
  end if
end sub

sub onSearchResponse(event)
  m.nodes.vodTask.unobserveFieldScoped("response")
  response = event.getData()

  root = createObject("roSGNode", "ContentNode")
  root.appendChild(response)

  if not isValid(m.nodes.results.content)
    FOCUS_pushNode(m.nodes.results)
  end if

  m.nodes.results.content = root
end sub

sub onResultSelected(event)
  selectedIndexSet = event.getData()
  LOG_TRACE("onResultSelected()", selectedIndex)

  SCENE_getRouter().callFunc("navigateTo", {
    id: "DetailScreen",
    data: {
      assetIndex: selectedIndexSet[1]
      assets: m.nodes.results.content.getChild(selectedIndexSet[0])
    }
  })
end sub