sub init()
  m.nodes = {
    video: m.top.findNode("video")
    heartbeat: m.top.findNode("heartbeat")
    vodTask: SCENE_getVODTask()
  }

  m.state = {
    vod: invalid
  }

  FOCUS_init([m.nodes.video])

  m.nodes.vodTask.control = "run"
  m.nodes.heartbeat.duration = m.global.appConfig.heartbeat_interval_seconds

  m.top.observeField("data", "onData")
  m.top.observeField("focusedChild", "onFocus")
  m.nodes.video.observeField("state", "onState")
  m.nodes.heartbeat.observeField("fire", "onHeartbeat")
end sub

sub onFocus()

  if m.top.hasFocus()
    FOCUS_restore()
  end if

end sub

function onKeyEvent(key, press) as boolean
  handled = false

  if press and true = m.state.isDeepLink

    if "back" = key
      SCENE_getRouter().callFunc("navigateTo", { id: "DetailScreen", data: m.top.data, clearHistory: true })
      handled = true
    end if

  end if

  return handled
end function

'''''''''
' onData: handles normal data flow and deep links
'
' @param {} event
'''''''''
sub onData(event)
  data = event.getData()

  if isValid(data.node)
    startPlayback(data.node)

  else if isValid(data.contentId) and isValid(data.mediaType)
    STATE_setState({ isDeepLink: true })
    m.nodes.vodTask.observeFieldScoped("response", "onVod")
    m.nodes.vodTask.request = {
      id: data.contentId
    }

  end if
end sub

'''''''''
' onVod: Asset is ready for playback
'
' @param {} event
'''''''''
sub onVod(event)
  vod = event.getData()
  startPlayback(vod)
end sub

'''''''''
' startPlayback: plays the video, currently hard-coded so all videos are the same
'
' @param {} data
'''''''''
sub startPlayback(data)
  STATE_setState({ vod: data })

  videoContent = CreateObject("roSGNode", "ContentNode")
  videoContent.contentType = data.type
  videoContent.title = data.title
  videoContent.description = data.description
  videoContent.releaseDate = data.year

  if m.global.appConfig.play_drm_stream
    videoContent.streamFormat = "dash"
    videoContent.url = "https://storage.googleapis.com/wvmedia/cenc/h264/tears/tears.mpd"
    videoContent.drmParams = {
      keySystem: "Widevine"
      licenseServerUrl: "https://proxy.uat.widevine.com/proxy?provider=widevine_test"
    }
  else
    videoContent.streamFormat = "hls"
    videoContent.url = "http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8"
  end if

  m.nodes.video.content = videoContent

  cachedProgress = getProgressFromCache(data.id)
  if isValid(cachedProgress)
    videoContent.bookmarkPosition = cachedProgress
  end if

  m.nodes.video.control = "play"
end sub

'''''''''
' onState: starts and stops the heartbeat based on video state
'
' @param {} event
'''''''''
sub onState(event)
  state = event.getData()
  if "playing" = state
    ' start heartbeat
    startHeartbeat()
  else if "paused" = state
    ' should the heartbeat be paused here?
  else if "stopped" = state
    stopHeartbeat()
  else if "finished" = state
    stopHeartbeat()
    SCENE_getRouter().callFunc("back", {})
  else if "error" = state
    stopHeartbeat()
    LOG_ERROR("Video playback error.", {
      code: m.nodes.video.errorCode
      message: m.nodes.video.errorMsg
      messageStr: m.nodes.video.errorStr
      info: m.nodes.video.info
    })
  end if
end sub

sub startHeartbeat()
  onHeartbeat()
  m.nodes.heartbeat.control = "start"
end sub

sub stopHeartbeat()
  m.nodes.heartbeat.control = "stop"
end sub

'''''''''
' onHeartbeat: calculates the progress percent and fires the heartbeat. After heartbeat is sent, caches the video progress (this could be split into its own timer for more granular control).
'
' @param {} event
'''''''''
sub onHeartbeat()

  if isValid(m.state.vod)
    id = m.state.vod.id
    progress = m.nodes.video.position
    progressPercent = int((progress / m.nodes.video.duration) * 100)

    m.nodes.vodTask.request = {
      id: id
      progress: progressPercent
    }

    updateProgressCache(id, progress)
  end if
end sub

'''''''''
' getProgressFromCache: Retrieves cache progress for this asset id
'
' @param {} id
' @return
'''''''''
function getProgressFromCache(id)
  cache = m.global.cache
  if isValid(cache)
    return m.global.cache[id]
  end if
  return invalid
end function

'''''''''
' updateProgressCache: updates cached progress for this asset
'
' @param {} id
' @param {} progress
'''''''''
sub updateProgressCache(id, progress)
  cache = m.global.cache

  if not isValid(cache)
    cache = {}
  end if

  cache[id] = progress
  GLOBAL_setField("cache", cache)
end sub