'''''''''
' init:
'
'''''''''
sub init()
end sub

'''''''''
' setup:
'
'''''''''
sub setup()
  m.state = {
    apiToken: invalid
    queuedRequest: invalid
  }
  m.const = {
    SERVER_ADDRESS: m.global.appConfig.api_address
  }
  getAPIToken()
end sub

'''''''''
' handleRequest: executes or queues requests
'
' @param {} message
'''''''''
sub handleRequest(message)
  if isValid(m.state.apiToken)
    executeRequest(message)
  else
    m.state.queuedRequest = message
  end if
end sub

'''''''''
' executeRequest: points request to correct endpoint function
'
' @param {} message
'''''''''
sub executeRequest(message)
  request = message.getData()

  if isValid(request)
    if isValid(request.search) or isValid(request.filter)
      getVod(request)

    else if isValid(request.id) and isValid(request.progress)
      postHeartbeat(request)

    else if isValid(request.id)
      getVodById(request)

    end if
  end if
end sub

'''''''''
' getAPIToken: root endpoint, fetches the API token and stores it
'
'''''''''
sub getAPIToken()
  LOG_TRACE("getAPIToken()")
  response = NETWORK_get(m.const.SERVER_ADDRESS)
  response = parseJson(response.getString())

  if isValid(response) and isValid(response.apiToken)
    STATE_setState({ apiToken: response.apiToken })

    ' Roku edge case:
    ' We can't specify header properties for Posters through its URI interface field. However, we can pull the HTTP agent and apply the header to that object. The Poster HTTP Agent is an object that ALL posters use by reference, so in a production project we would likely only want to set this temporarily so that we don't inadvertently send the api token to unexpected destinations.
    poster = createObject("roSGNode", "Poster")
    posterHttpAgent = poster.getHttpAgent()
    posterHttpAgent.addHeader("x-api-token", response.apiToken)
    m.top.ready = true

    if isValid(m.state.queuedRequest)
      executeRequest(m.state.queuedRequest)
      STATE_setState({ queuedRequest: invalid })
    end if
  end if
end sub

'''''''''
' getVodById: /vod/{id} endpoint
'
' @param {} request
'''''''''
sub getVodById(request)
  LOG_TRACE("getVodById()", request)

  headers = { "x-api-token": m.state.apiToken }
  url = m.const.SERVER_ADDRESS + "/vod/" + request.id

  response = NETWORK_get(url, headers)
  response = parseJson(response.getString())
  m.top.response = createVODNode(response)
end sub

'''''''''
' getVod: /vod endpoint
'
' @param {} request
'''''''''
sub getVod(request)
  LOG_TRACE("getVod()", request)

  headers = { "x-api-token": m.state.apiToken }
  url = m.const.SERVER_ADDRESS + "/vod/search"

  if isValid(request.search)
    url = NETWORK_addQueryParam(url, "s=" + request.search)
  end if
  if isValid(request.filter)
    url = NETWORK_addQueryParam(url, "filter=" + request.filter)
  end if

  response = NETWORK_get(url, headers)
  response = parseJson(response.getString())

  responseNode = createObject("roSGNode", "ContentNode")
  for each vod in response
    responseNode.appendChild(createVODNode(vod))
  end for

  m.top.response = responseNode
end sub

'''''''''
' postHeartbeat:
'
' @param {} request
'''''''''
sub postHeartbeat(request)
  LOG_TRACE("postHeartbeat()", request)

  headers = { "x-api-token": m.state.apiToken }
  url = m.const.SERVER_ADDRESS + "/vod/heartbeat"
  body = {
    id: request.id
    progress: request.progress
  }

  NETWORK_post(url, headers, body)
end sub

'''''''''
' createVODNode: creates a VOD data node, handles mocking data if necessary
'
' @param {} data
' @return
'''''''''
function createVODNode(data)
  vodNode = CreateObject("roSGNode", "VODDataNode")

  if true = m.global.appConfig.mock_missing_data
    if not isValid(data.title)
      data.title = RANDOM_word(true) + " " + RANDOM_word(true)
    end if

    if not isValid(data.description)
      data.description = RANDOM_paragraphs(2)
    end if

    if not isValid(data.year)
      data.year = RANDOM_year("19YY")
    end if
  end if

  vodNode.setFields(data)
  return vodNode
end function