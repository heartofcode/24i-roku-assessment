'''''''''
' init: Runs some basic initialization tasks. Using this to prepare the VOD Task, in other scenarios this might check if the user is logged in.
'
'''''''''
sub init()
  m.nodes = {
    vodTask: SCENE_getVodTask()
  }
  m.nodes.vodTask.observeField("ready", "onSceneReady")
  m.nodes.vodTask.control = "run"
end sub

'''''''''
' onSceneReady: Attempts to deep link, otherwise renders the Grid Screen
'
'''''''''
sub onSceneReady()
  m.nodes.vodTask.unobserveField("ready")
  if false = openDeepLink(m.top.deepLink)
    SCENE_getRouter().callFunc("navigateTo", { id: "GridScreen" })
  end if

  m.top.observeField("deepLink", "onDeepLink")
end sub

'''''''''
' onKeyEvent: Handles back navigation and presenting the exit dialog
'
' @param {} key
' @param {} press
' @return {boolean}
'''''''''
function onKeyEvent(key, press) as boolean
  handled = false

  if press

    if "back" = key
      if not SCENE_getRouter().callFunc("back", {})
        exitAppDialog()
        handled = true
      end if
    end if

  end if

  return handled
end function

'''''''''
' onDeepLink
'
' @param {} event
'''''''''
sub onDeepLink(event)
  deepLink = event.getData()
  LOG_TRACE("Deep link", deepLink)

  openDeepLink(deepLink)
end sub

'''''''''
' openDeepLink: opens a deep link!
'
' @param {} link
' @return
'''''''''
function openDeepLink(link)
  success = false

  if isValid(link)
    if "movie" = link.mediaType
      success = true
      SCENE_getRouter().callFunc("navigateTo", {
        id: "PlayerScreen"
        data: {
          contentId: link.contentId
          mediaType: link.mediaType
        }
        clearHistory: true
      })
    else if "series" = link.mediaType
      success = true
      SCENE_getRouter().callFunc("navigateTo", {
        id: "DetailScreen"
        data: {
          contentId: link.contentId
          mediaType: link.mediaType
        }
        clearHistory: true
      })
    end if
  end if

  return success
end function