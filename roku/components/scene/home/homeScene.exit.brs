
'''''''''
' exitAppDialog: renders the exit dialog
'
'''''''''
sub exitAppDialog()
  m.dialog = createObject("roSGNode", "Dialog")
  m.dialog.update({
    title: "Are you sure you want to exit?"
    buttons: ["Exit", "No"]
  })
  m.dialog.observeField("buttonSelected", "onExitButton")
  m.dialog.observeField("wasClosed", "exitApp")

  m.top.dialog = m.dialog
end sub

'''''''''
' onExitButton: handles the dialog buttons
'
' @param {} event
'''''''''
sub onExitButton(event)
  buttonIndex = event.getData()

  if 0 = buttonIndex
    exitApp()
  else
    m.dialog.unobserveField("buttonSelected")
    m.dialog.unobserveField("wasClosed")
    m.dialog.close = true
  end if
end sub

'''''''''
' exitApp: sets the exitApp field on m.top, which is being observed in main.brs
'
'''''''''
sub exitApp()
  m.top.exitApp = true
end sub