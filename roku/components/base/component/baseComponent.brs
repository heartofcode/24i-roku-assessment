'''''''''
' init:
'
'''''''''
sub init()
end sub

'''''''''
' show: The component is being rendered.
'
'''''''''
sub show()
  prepare()
end sub

'''''''''
' prepare: Performs any data collection the component requires
'
'''''''''
sub prepare()
end sub

'''''''''
' ready: Component is ready to be made visible and interactable to the user
'
'''''''''
sub ready()
end sub