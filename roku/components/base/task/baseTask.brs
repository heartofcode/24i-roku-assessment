'''''''''
' init:
'
'''''''''
sub init()
  m.top.id = m.top.getSubtype()
  m.top.functionName = "loop"

  m.urlTransfer = createObject("roURLTransfer")
  m.port = createObject("roMessagePort")
  m.top.observeField("request", m.port)
end sub

'''''''''
' loop: Handles the basic task loop logic.
'
'''''''''
sub loop()
  setup()
  while true

    msg = wait(0, m.port)
    msgType = type(msg)

    if "roSGNodeEvent" = msgType
      if "request" = msg.getField()
        handleRequest(msg)
      end if
    end if

  end while
end sub

'''''''''
' setup: Function available to override for performing actions prior to the event loop.
'
'''''''''
sub setup()
end sub

'''''''''
' handleRequest: Function available to override for responding to events.
'
' @param {} event
'''''''''
sub handleRequest(event)
  LOG_WARN("Override the handleEvent function.")
end sub